# Types

Le javascript est un langage non typé. C'est à dire que le type de la variable devient le type de sa valeur.

Si une variable est valorisée avec une chaine de caractère, alors elle sera interprétée comme une chaine de caractères.

De même, cette même variable peut être valorisée par la suite avec un nombre entier, et sera interprétée non plus comme une chaine
de caractères mais comme un nombre entier.

## Contante

```js
const nombreConstant = 34

const chaineDeCaractèresConstante = 'Je suis constante'
```

> Noter que la constante est valorisée dès sa définition.

## Variable

```js
let nombreVariable = 34

ou

let nombreVariable;
nombreVariable = 34
```

> Noter que la variable peut être définie puis valorisée ultérieurement dans le code.

> Noter également que le mot clé `var` n'est plus utilisé au profit de `let` dans la nouvelle version de javascript (ES6).

## Chaîne de caractères (string)

```js
let chaineCaractère = 'voici une chaine de caractères'

ou

let chaineCaractère = "voici une autre chaine de caractères"
```

> Noter qu'une chaine de caractères peut être définie par des `'` ou des `"`.
>
> Cependant, la notation `'` est privilégiée.

## Nombre entier et decimal

```js
let nombreEntier = 45

let nombreDecimal = 3.5
```

## Booléen

```js
let booleen = true

let booleen = false
```

## Objet

```js
let objet = {
  nom: 'John',
  prenom: 'Doe'
}
```

> En javascript, les objets peuvent être créés dynamiquement.

## Tableau (array)

```js
let tableau = [] // tableau vide

let tableau = [50, 20]

let tableau = ['cinquante', 'vingt']

let tableau = [50, 'vingt']
```

> Un tableau peut contenir des valeurs de type différents.