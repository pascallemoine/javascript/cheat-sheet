# Opérateurs logique

## Egalité

```js
const ageNombre = 18
const ageString = '18'

if (ageNombre == ageString) {
  console.log('Même valeur')
}
```

> Noter l'utilisation de l'opérateur `==` qui vérifie si la valeur est la même. Ici `18` et `'18'` sont
concidérés comme étant identiques. Le type de la valeur n'est pas pris en compte.

## Egalité stricte

```js
const ageNombre = 18
const ageString = '18'

if (ageNombre === ageString) {
  console.log('Valeur différente')
}
```

> Noter l'utilisation de l'opérateur `===` qui vérifie d'abord si les types sont identiques puis si les valeurs sont identiques.
Ici, `18` est un nombre et `'18'` est une chaine de caractère. Donc l'égalité est fausse malgré leur valeurs identiques.

## Différence et différence stricte

```js
const age1 = 18
const age2 = 20

if (age1 !== age2) {
  console.log('Ages différents')
}

ou

const age3 = '18'

if (age1 != age3) {
  console.log('Ages identiques')
}
```

> La différence fonctionne à l'identique de l'égalité. On peut demander la différence sur les valeurs uniquement sans comparer le type `!=`.
Ou demande la différence en comparant le type `!==`.
