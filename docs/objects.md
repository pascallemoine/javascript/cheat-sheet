# Objets

## Créer un objet

```js
let objet = {} // objet vide

let objet = {
  nom: 'John',
  prenom: 'Doe'
}

let objet = {
  books: [],
  addBook(book) {
    books.push(book)
  }
}
```

## Ajouter un attribut

```js
let objet = {
  nom: 'John'
}

objet.prenom = 'Doe'
ou
objet['prenom'] = 'Doe'

/*
  objet est maintenant égal à
  {
    nom: 'John',
    prenom: 'Doe'
  }
*/
```

## Lire un attribut

```js
let objet = {
  nom: 'John',
  prenom: 'Doe'
}

console.log(objet.nom)
ou
console.log(objet['nom'])
```

## Appeler une fonction sur un objet

```js
let objet = {
  books: [],
  addBook(book) {
    books.push(book)
  }
}

objet.addBook('1984')
ou
objet['addBook']('1984')
```