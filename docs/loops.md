# Boucles

## for i

```js
const tab = [42, 90]

for(let i = 0; i < tab.length; i++) {
  console.log(tab[i])
}
```

## for..of

```js
const tab = [42, 90]

for(let nombre of tab) {
  console.log(nombre)
}
```

## for each

```js
const tab = [42, 90]

tab.forEach(function(nombre) {
  console.log(nombre)
})
```

> La fonction passée en paramètre du forEach est appelée autant de fois qu'il y a de ligne avec
l'argument `nombre` qui prend la valeur de la ligne en cours

## for..in

```js
const objet = {
  nom: 'John',
  prenom: 'Doe'
}

for(let attribut in objet) {
  console.log(attribut, objet[attribut]) // cf 'Lire un attribut' dans la section 'Objets'
  // écrit en console :
  // nom John
  // prenom Doe
}
```

> Attention à ne pas confondre avec le for of qui boucle dans les tableaux

## while

```js
const tab = [42, 90]

let i = 0
while(i < tab.length) {
  console.log(tab[i])
  i++
}
```

> Attention à bien incrémenter l'index `i` sinon on à boucle à l'infini !

## do..while

```js
const tab = [42, 90]

let i = 0
do {
  console.log(tab[i])
  i++
} while(i < tab.length)
```