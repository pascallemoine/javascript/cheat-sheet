# Fonctions

## Définir une fonction

```js
function maFonction() {
  // code de la fonction
}

function maFonctionAvecParametres(parametre) {
  console.log(parametre)
}
```

> Noter une nouvelle fois que les paramètres ne sont pas typés et peuvent donc contenir n'importe quel type de valeur.

## Appeler une fonction

```js

maFonction()

maFonctionAvecParametres(42)
// écrit dans le terminal : 42
```

## Retourner une valeur

```js
function addition(a, b) {
  return a + b
}

const resultat = addition(1, 2)
```