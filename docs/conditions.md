# Conditions

## if

```js
if (true) {
  console.log('Condition vérifiée')
}
```

## if..else

```js
const age = 18

if (age < 18) {
  console.log('Mineur')
} else {
  console.log('Majeur)
}
```

## if..elseif..else

```js
const age = 18

if (age < 14) {
  console.log('Enfant')
} else if (age < 18) {
  console.log('Adolescent')
} else {
  console.log('Adulte')
}
```

## if ternaire

```js
const age = 18

age < 18 ? console.log('Mineur') : console.log('Majeur')
```

> Noter l'utilisation des caractères `?` pour les instructions true et `:` pour les instructions false.